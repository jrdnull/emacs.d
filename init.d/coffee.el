(use-package coffee-mode
  :config
  (add-to-list 'coffee-args-compile "--no-header")
  (subword-mode +1)
   (setq coffee-tab-width 4))
